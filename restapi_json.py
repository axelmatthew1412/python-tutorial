from flask import Flask, jsonify, request
import json
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello, World!"

@app.route("/get_json", methods=["GET"])
def get_json():
    data = {
        "name": "John",
        "age": 30,
        "city": "New York"
    }
    return json.dumps(data)

@app.route("/post_json", methods=["POST"])
def post_json():
    data = request.get_json()
    name = data["name"]
    age = data["age"]
    city = data["city"]
    return jsonify(name=name, age=age, city=city)

if __name__ == "__main__":
    app.run(debug=True)
